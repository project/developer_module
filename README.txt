"Developer Module" 

After installing this module, you can easily configure theme debug 
as well as disable cache feaure during development option from Admin interface.

When you enable "Enable Theme Debug" option from DEVELOPER CONFIGURATION 
then it will suggest template of the website. 
For that, please view sourse of the webpage.

When you enable "Disable Cache During Development" option 
from DEVELOPER CONFIGURATION then it is not required to clear cache every time 
for modification of your template and module file. 
For that, please login as Administrator and modify any twig file, 
it will automatically change that content for that authenticated user.

Please note that we are developing this module 
so we will update this readme file whenever there will be any change requires.
